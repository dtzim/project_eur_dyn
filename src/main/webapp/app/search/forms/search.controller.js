(function() {
    'use strict';

    angular
        .module('e2EApp')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['$scope','$state', '$http'];

    function SearchController ($scope, $state, $http) {
        var vm = this;
        var selectedCriteria = "lastName";
        var asc = true;

        vm.doSearch = function() {
            // Exercise 2
            //Change the search functionality to also take the user's email
            //we need to change the call with params.
            // differently we cannot pass the email correctly
            $http({
                method: 'GET',
                url: "http://localhost:8080/api/search",
                params: {
                            query: vm.search.searchTerm,
                            selCriteria: selectedCriteria,
                            ascending: asc
                        }
              }).then(function successCallback(response) {
                    if (response.status == 200) {
                        vm.search.users = response.data
                    }
                }, function errorCallback(response) {

                });

        }


        // Exercise 3
        // Implement a new button on the GUI called "Clear". By clicking on "Clear" the entered text in the searchfield should be deleted. If the results table contains any results then they should be removed from the GUI
        vm.clear = function() {
            vm.search.users = null;
            vm.search.searchTerm = null;
            selectedCriteria = "lastName";
            asc = true;
        };

        vm.sortByHeader = function(criteria) {

                if( criteria === selectedCriteria){
                    asc = !asc;
                }else{
                    asc = true;
                    selectedCriteria = criteria;
                }
                // Exercise 2
                //Change the search functionality to also take the user's email
                //we need to change the call with params.
                // differently we cannot pass the email correctly
                $http({
                    method: 'GET',
                     url: "http://localhost:8080/api/search/",
                     params: {
                        query: vm.search.searchTerm,
                        selCriteria: selectedCriteria,
                        ascending: asc
                     }
                }).then(function successCallback(response) {
                    if (response.status == 200) {
                        vm.search.users = response.data
                    }
                });
        };

    }
})();
